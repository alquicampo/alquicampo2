package com.sadycr.alquicampo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sadycr.alquicampo.Adapatadores.ListViewMaquinariaAdapter;
import com.sadycr.alquicampo.Models.Maquinaria;
import com.sadycr.alquicampo.admin.PropietarioActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

public class MaquinariaActivity extends AppCompatActivity {
    private ArrayList<Maquinaria> listCarga = new ArrayList<Maquinaria>();
    ArrayAdapter<Maquinaria> arrayAdapterCarga;
    ListViewMaquinariaAdapter listViewCargaAdapter;
    LinearLayout linearLayoutEditar;
    ListView listViewM;

    Toolbar toolbar;

    EditText inputPropietario, inputTelefono, inputMaquinaria, inputModelo, inputMotor, inputAlquiler, inputMensaje;
    Button btnCancelar;

    Maquinaria maquinariaSeleccionada;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maquinaria);

        inputPropietario = findViewById(R.id.inputPropietario);
        inputTelefono = findViewById(R.id.inputTelefono);
        inputMaquinaria = findViewById(R.id.inputMaquinaria);
        inputModelo = findViewById(R.id.inputModelo);
        inputMotor = findViewById(R.id.inputMotor);
        inputAlquiler = findViewById(R.id.inputAlquiler);
        inputMensaje = findViewById(R.id.inputMensaje);
        btnCancelar = findViewById(R.id.btnCancelar);


        toolbar = findViewById(R.id.toolsbar);
        setSupportActionBar(toolbar);

        listViewM = findViewById(R.id.listViewCarga);
        linearLayoutEditar = findViewById(R.id.linearLayoutEditar);

        listViewM.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                maquinariaSeleccionada = (Maquinaria) parent.getItemAtPosition(position);
                inputPropietario.setText(maquinariaSeleccionada.getPropietario());
                inputTelefono.setText(maquinariaSeleccionada.getTelefono());
                inputMaquinaria.setText(maquinariaSeleccionada.getMaquinaria());
                inputModelo.setText(maquinariaSeleccionada.getModelo());
                inputMotor.setText(maquinariaSeleccionada.getMotor());
                inputAlquiler.setText(maquinariaSeleccionada.getAlquiler());
                inputMensaje.setText(maquinariaSeleccionada.getMensaje());

                linearLayoutEditar.setVisibility(View.VISIBLE);
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutEditar.setVisibility(View.GONE);
                maquinariaSeleccionada = null;
            }
        });

        inicializarFirebase();
        listarCarga();
    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    private void listarCarga() {
        databaseReference.child("Maquinaria").orderByChild("timestamp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listCarga.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {
                    Maquinaria c = objSnaptshot.getValue(Maquinaria.class);
                    listCarga.add(c);
                }
                listViewCargaAdapter = new ListViewMaquinariaAdapter(MaquinariaActivity.this, listCarga);
                listViewM.setAdapter(listViewCargaAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin, menu);

        MenuItem menuItem = menu.findItem(R.id.buscar);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listViewCargaAdapter.getFilter().filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String propietario = inputPropietario.getText().toString();
        String telefono = inputTelefono.getText().toString();
        String maquinaria = inputMaquinaria.getText().toString();
        String modelo = inputModelo.getText().toString();
        String motor = inputMotor.getText().toString();
        String alquiler = inputAlquiler.getText().toString();
        String adicional = inputMensaje.getText().toString();
        switch (item.getItemId()) {

            case R.id.menu_agregar:
                insertar();
                break;
            case R.id.menu_guardar:
                if (maquinariaSeleccionada != null) {
                    if (validarInputs() == false) {
                        Maquinaria p = new Maquinaria();
                        p.setIdmaquinaria(maquinariaSeleccionada.getIdmaquinaria());
                        p.setPropietario(propietario);
                        p.setTelefono(telefono);
                        p.setMaquinaria(maquinaria);
                        p.setModelo(modelo);
                        p.setMotor(motor);
                        p.setAlquiler(alquiler);
                        p.setMensaje(adicional);
                        p.setFecharegistro(maquinariaSeleccionada.getFecharegistro());
                        p.setTimestamp(maquinariaSeleccionada.getTimestamp());
                        databaseReference.child("Maquinaria").child(p.getIdmaquinaria()).setValue(p);
                        Toast.makeText(this, "Actualizado Correctamente", Toast.LENGTH_LONG).show();
                        linearLayoutEditar.setVisibility(View.GONE);
                        maquinariaSeleccionada = null;
                    }
                } else {
                    Toast.makeText(this, "Seleccione una Carga", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.menu_eliminar:
                if (maquinariaSeleccionada != null) {
                    Maquinaria c2 = new Maquinaria();
                    c2.setIdmaquinaria(maquinariaSeleccionada.getIdmaquinaria());
                    databaseReference.child("Maquinaria").child(c2.getIdmaquinaria()).removeValue();
                    linearLayoutEditar.setVisibility(View.GONE);
                    maquinariaSeleccionada = null;
                    Toast.makeText(this, "Eliminado Correctamente", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Seleccione una Carga para eliminar", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.home:
                home();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void home() {
        Intent intenthome = new Intent(this, PropietarioActivity.class);
        startActivity(intenthome);
    }

    public void insertar() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(
                MaquinariaActivity.this
        );
        View mView = getLayoutInflater().inflate(R.layout.maquinaria, null);
        Button btnInsertar = (Button) mView.findViewById(R.id.btnInsertar);
        final EditText mInputPropietario = (EditText) mView.findViewById(R.id.inputPropietario);
        final EditText mInputTelefono = (EditText) mView.findViewById(R.id.inputTelefono);
        final EditText mInputMaquinaria = (EditText) mView.findViewById(R.id.inputMaquinaria);
        final EditText mInputModelo = (EditText) mView.findViewById(R.id.inputModelo);
        final EditText mInputMotor = (EditText) mView.findViewById(R.id.inputMotor);
        final EditText mInputAlquiler = (EditText) mView.findViewById(R.id.inputAlquiler);
        final EditText mInputMensaje = (EditText) mView.findViewById(R.id.inputMensaje);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String propietario = mInputPropietario.getText().toString();
                String telefono = mInputTelefono.getText().toString();
                String maquinaria = mInputMaquinaria.getText().toString();
                String modelo = mInputModelo.getText().toString();
                String motor = mInputMotor.getText().toString();
                String alquiler = mInputAlquiler.getText().toString();
                String adicional = mInputMensaje.getText().toString();
                if (propietario.isEmpty() || propietario.length() < 3) {
                    showError(mInputPropietario, "Nombre invalido (Min. 3 letras)");
                } else if (telefono.isEmpty() || telefono.length() < 10) {
                    showError(mInputTelefono, "Telefono invalido (Min. 10 números)");
                } else if (maquinaria.isEmpty() || maquinaria.length() <3 ) {
                    showError(mInputMaquinaria, "Nombre invalido (Min. 3 letras)");
                } else if (modelo.isEmpty() || modelo.length() < 3) {
                    showError(mInputModelo, "Nombre invalido (Min. 3 letras)");
                } else if (motor.isEmpty() || motor.length() < 3) {
                    showError(mInputMotor, "Nombre invalido (Min. 3 letras)");
                } else if (alquiler.isEmpty() || alquiler.length() < 3) {
                    showError(mInputAlquiler, "Nombre invalido (Min. 3 letras)");
                } else if (adicional.isEmpty() || adicional.length() < 3) {
                    showError(mInputMensaje, "Nombre invalido (Min. 3 letras)");
                } else {
                    Maquinaria p = new Maquinaria();
                    p.setIdmaquinaria(UUID.randomUUID().toString());
                    p.setPropietario(propietario);
                    p.setTelefono(telefono);
                    p.setMaquinaria(maquinaria);
                    p.setModelo(modelo);
                    p.setMotor(motor);
                    p.setAlquiler(alquiler);
                    p.setMensaje(adicional);
                    p.setFecharegistro(getFechaNormal(getFechaMilisegundos()));
                    p.setTimestamp(getFechaMilisegundos() * -1);
                    databaseReference.child("Maquuinaria").child(p.getIdmaquinaria()).setValue(p);
                    Toast.makeText(MaquinariaActivity.this, "Registrado Correctamente", Toast.LENGTH_LONG).show();

                    dialog.dismiss();
                }
            }
        });


    }

    public void showError(EditText input, String s) {
        input.requestFocus();
        input.setError(s);
    }

    public long getFechaMilisegundos() {
        Calendar calendar = Calendar.getInstance();
        long tiempounix = calendar.getTimeInMillis();

        return tiempounix;
    }

    public String getFechaNormal(long fechamilisegundos) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        String fecha = sdf.format(fechamilisegundos);
        return fecha;
    }

    public boolean validarInputs() {
        String propietario = inputPropietario.getText().toString();
        String telefono = inputTelefono.getText().toString();
        String maquinaria = inputMaquinaria.getText().toString();
        String modelo = inputModelo.getText().toString();
        String motor = inputMotor.getText().toString();
        String alquiler = inputAlquiler.getText().toString();
        String adicional = inputMensaje.getText().toString();
        if (propietario.isEmpty() || propietario.length() < 3) {
            showError(inputPropietario, "Nombre invalido. (Min 3 letras)");
            return true;
        } else if (telefono.isEmpty() || telefono.length() < 9) {
            showError(inputTelefono, "Telefono invalido (Min 9 números)");
            return true;
        } else if (maquinaria.isEmpty() || maquinaria.length() < 3) {
            showError(inputMaquinaria, "Nombre invalido (Min 3 letras)");
            return true;
        } else if (modelo.isEmpty() || modelo.length() < 3) {
            showError(inputModelo, "Nombre invalido (Min 3 letras)");
            return true;
        } else if (motor.isEmpty() || motor.length() < 3) {
            showError(inputMotor, "Nombre invalido (Min 3 letras)");
            return true;
        } else if (alquiler.isEmpty() || alquiler.length() < 3) {
            showError(inputAlquiler, "Nombre invalido (Min 3 letras)");
            return true;
        } else if (adicional.isEmpty() || adicional.length() < 3) {
            showError(inputAlquiler, "Nombre invalido (Min 3 letras)");
            return true;
        } else {
            return false;
        }
    }
}

