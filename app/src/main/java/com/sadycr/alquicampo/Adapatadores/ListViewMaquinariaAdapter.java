package com.sadycr.alquicampo.Adapatadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.sadycr.alquicampo.FIltroBuscador.CustomFilterMaquinaria;
import com.sadycr.alquicampo.Models.Maquinaria;
import com.sadycr.alquicampo.R;

import java.util.ArrayList;

public class ListViewMaquinariaAdapter extends BaseAdapter implements Filterable {

    Context context;
    public ArrayList<Maquinaria> maquinariaData, filterList;
    LayoutInflater layoutInflater;
    Maquinaria maquinariamodel;
    CustomFilterMaquinaria filterM;

    public ListViewMaquinariaAdapter(Context context, ArrayList<Maquinaria> maquinariaData){
        this.context = context;
        this.maquinariaData = maquinariaData;
        this.filterList = maquinariaData;
        layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );
    }


    @Override
    public int getCount() {
        return maquinariaData.size();
    }

    @Override
    public Object getItem(int position) {
        return maquinariaData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View romView = convertView;
        if (romView==null){
            romView = layoutInflater.inflate(R.layout.lista_maquinaria,
                    null,
                    true);
        }

        TextView propietario = romView.findViewById(R.id.propietario);
        TextView telefono = romView.findViewById(R.id.telefono);
        TextView maquinaria = romView.findViewById(R.id.maquinaria);
        TextView modelo = romView.findViewById(R.id.modelo);
        TextView motor = romView.findViewById(R.id.motor);
        TextView alquiler = romView.findViewById(R.id.alquiler);
        TextView adicional = romView.findViewById(R.id.mensaje);

        maquinariamodel = maquinariaData.get(position);
        propietario.setText(maquinariamodel.getPropietario());
        telefono.setText(maquinariamodel.getTelefono());
        maquinaria.setText(maquinariamodel.getMaquinaria());
        modelo.setText(maquinariamodel.getModelo());
        motor.setText(maquinariamodel.getMotor());
        alquiler.setText(maquinariamodel.getAlquiler());
        adicional.setText(maquinariamodel.getMensaje());

        return romView;
    }

    @Override
    public Filter getFilter() {
        if (filterM==null){
            filterM = new CustomFilterMaquinaria(filterList, this);
        }
        return filterM;
    }
}