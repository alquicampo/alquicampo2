package com.sadycr.alquicampo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sadycr.alquicampo.admin.PropietarioActivity;

import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {

    EditText et_name, et_mail, et_pass;
    Button btn_registrar, btn_inicio;
    AppCompatButton btninicio;
    boolean valid = true;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    CheckBox propietario,agricultor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        principal();

        et_name = findViewById(R.id.et_name);
        et_mail = findViewById(R.id.et_mail);
        et_pass = findViewById(R.id.et_pass);

        propietario = findViewById(R.id.propietario);
        agricultor = findViewById(R.id.agricultor);

        agricultor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()){
                    propietario.setChecked(false);
                }
            }
        });

        propietario.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()){
                    agricultor.setChecked(false);
                }
            }
        });

        btn_registrar = findViewById(R.id.btn_registrar);
        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registro(et_name);
                registro(et_mail);
                registro(et_pass);

                if (!(propietario.isChecked() || agricultor.isChecked())){
                    Toast.makeText(RegistroActivity.this, "Seleccione el tipo de cuenta", Toast.LENGTH_SHORT).show();
                    return;
                }



                if (valid) {
                    firebaseAuth.createUserWithEmailAndPassword(et_mail.getText().toString(), et_pass.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            Toast.makeText(RegistroActivity.this, "Usuario Creado", Toast.LENGTH_SHORT).show();
                            DocumentReference df = firebaseFirestore.collection("Usuarios").document(user.getUid());
                            Map<String, Object> userInfo = new HashMap<>();
                            userInfo.put("Nombre Completo", et_name.getText().toString());
                            userInfo.put("Correo Electronico", et_mail.getText().toString());

                            if (propietario.isChecked()){
                                userInfo.put("propietario","1");
                                ;                            }
                            if (agricultor.isChecked()){
                                userInfo.put("agricultor", "1");
                            }

                            df.set(userInfo);
                            if (propietario.isChecked()){
                                startActivity(new Intent(getApplicationContext(), PropietarioActivity.class));
                                finish();
                            }
                            if (agricultor.isChecked()){
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(RegistroActivity.this, "No se pudo crear la cuenta", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });

    }

    public boolean registro(EditText textregistro) {
        if (textregistro.getText().toString().isEmpty()) {
            textregistro.setError("Error");
            valid = false;
        } else {
            valid = true;
        }
        return valid;
    }
    private void principal() {
        btninicio = findViewById(R.id.btn_inicio);
        btninicio.setOnClickListener((evt) -> {
            onInicioClick();
        });
    }
    private void onInicioClick(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}