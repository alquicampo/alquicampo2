package com.sadycr.alquicampo.FIltroBuscador;

import android.widget.Filter;

import com.sadycr.alquicampo.Adapatadores.ListViewMaquinariaAdapter;
import com.sadycr.alquicampo.Models.Maquinaria;

import java.util.ArrayList;


public class CustomFilterMaquinaria extends Filter {
    ArrayList<Maquinaria> filterlist;
    ListViewMaquinariaAdapter adapter;

    public CustomFilterMaquinaria (ArrayList<Maquinaria> filterlist, ListViewMaquinariaAdapter adapter) {
        this.filterlist = filterlist;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constrain) {
        FilterResults results = new FilterResults();
        if (constrain!=null &&constrain.length()>0){
            constrain = constrain.toString().toUpperCase();
            ArrayList<Maquinaria> modeloFiltrado = new ArrayList<>();
            for(int i=0;i<filterlist.size();i++){
                if(filterlist.get(i).getPropietario().toUpperCase().contains(constrain) || filterlist.get(i).getTelefono().toUpperCase().contains(constrain)
                        || filterlist.get(i).getMaquinaria().toUpperCase().contains(constrain) || filterlist.get(i).getModelo().toUpperCase().contains(constrain)
                        || filterlist.get(i).getAlquiler().toUpperCase().contains(constrain) || filterlist.get(i).getMensaje().toUpperCase().contains(constrain)) {
                    modeloFiltrado.add(filterlist.get(i));
                }
            }
            results.count = modeloFiltrado.size();
            results.values = filterlist;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.maquinariaData = (ArrayList<Maquinaria>) results.values;
        adapter.notifyDataSetChanged();
    }
}
